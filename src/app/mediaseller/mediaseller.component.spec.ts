import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediasellerComponent } from './mediaseller.component';

describe('MediasellerComponent', () => {
  let component: MediasellerComponent;
  let fixture: ComponentFixture<MediasellerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediasellerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediasellerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
