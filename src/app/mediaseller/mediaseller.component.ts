import { Component, OnInit, ElementRef, NgZone, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiserviceService } from '../service/apiservice.service';
import { FormControl } from '@angular/forms';

import { MapsAPILoader } from '@agm/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../class/user';
import { Router } from '@angular/router';
import { Md5 } from 'md5-typescript';
import { AdversiterComponent } from '../adversiter/adversiter.component';
@Component({
  selector: 'app-mediaseller',
  templateUrl: './mediaseller.component.html',
  styleUrls: ['./mediaseller.component.css']
})
export class MediasellerComponent implements OnInit {
  baseUrl = "https://staging.gainbuzz.com:8080/gainbuzz/";
  categoryObj: any;
  userData:any;
  selAm:string;
  mediaForm: FormGroup;
  submitted:boolean;
  categoryArr = [];
  UserDetail:any;
  market:string;
  error:string;
  formData:any;
  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;
  categoryId:number;
  assetAdOptionId:number;

  typeId:number;
  @ViewChild("search",{static:false})
  public searchElementRef: ElementRef;


  constructor(private formBuilder: FormBuilder, private apiservice: ApiserviceService, private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone, private NgbActiveModal: NgbActiveModal, private router: Router, private modalService:NgbModal) { }
  
  ngOnInit() {
    this.mediaForm = this.formBuilder.group({
      businessName:['', Validators.required],
      selCategory:['', Validators.required],
      // searchControl:['', Validators.required]
    });
    this.mediaSeller(); 
    
    
    this.zoom = 4;
    this.latitude = 39.8282;
    this.longitude = -98.5795;

    //create search FormControl
    this.searchControl = new FormControl();

    //set current position
    
    
    var options = {
      types: ['(cities)'],
      // componentRestrictions: {country: this.userData.countryCode}
      componentRestrictions: {country: "IN"}
     };
    //load Places Autocomplete

    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, options);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          var place: google.maps.places.PlaceResult = autocomplete.getPlace();
          var PlaceResult = place.address_components;
          var city  = "";
          var state = "";
          for (let index = 0; index < PlaceResult.length; index++) {
            const element = PlaceResult[index];
            for (let i = 0; i < element.types.length; i++) {
              var typesele = element.types[i];
              if(typesele == "locality"){
                city = element.long_name;
              }
              if(typesele == "administrative_area_level_1"){
                state = element.long_name;
              }
            }
          }
          this.market = city+"-"+state;
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
        });
      });
    });


  }

  getCategory(event) {
		const selectEl = event.target;
		this.categoryId = selectEl.options[selectEl.selectedIndex].getAttribute('data-categoryId');
		this.assetAdOptionId = selectEl.options[selectEl.selectedIndex].getAttribute('data-assetCategoryId');
	}

  mediaSeller() {
		var apiUrl = this.baseUrl + "/guest/getAllAdOptionForSelect";
		var isAuth = false;
		this.apiservice.get(apiUrl, isAuth).subscribe(
			category => {
        category.data.map(item => {
          item.catId = 4
          var data = item.assetCategory[0];
          data.catId = item.id;
          return data;
        }).forEach(item => this.categoryArr.push(item))
    });
    
	}

  get m() { return this.mediaForm.controls; }

  onSubmitMedia(){
    this.formData = this.mediaForm.value;

    this.submitted = true;
		if (this.mediaForm.invalid) {
			return;
    }
    var apiUrl = this.baseUrl + "guest/registration";
    var isAuth = false;
    var userObj = new User();
    userObj.firstName  = this.userData.firstName;
    userObj.lastName  = this.userData.lastName;
    userObj.contactNo  = this.userData.number;
    userObj.email  = this.userData.email;
    userObj.password  = Md5.init(this.userData.password);
    userObj.ip  = "182.72.105.154";
    userObj.country = {
      id:this.userData.country
    };
    userObj.deviceId = "";
    userObj.organization = {
      id:0,
      name:this.userData.firstName
    };
    userObj.userRole = {id:"2"};
    userObj.deviceType = "WEB";
    userObj.mapTo = "USER";
    userObj.isFirstTimeLogin = true;
    userObj.isMobileNoVerified= false;
    userObj.isEmailVerified = true;
    userObj.market=[
      this.market
    ]

    
    userObj.userAssetCategory=[{
      assetTypeId: this.categoryId,
      adoptionId: this.assetAdOptionId,
      categoryId: this.formData.selCategory,
    }];
    
    this.apiservice.post(apiUrl, userObj, isAuth).subscribe(
			data => {
				this.UserDetail = data;
				if (data.status == 'error') {
					if (data.code == "NOT_FOUND") {
            this.error = data.message;
					}
					else {
            this.error = "User already Exits";
					}
				} else {
          this.NgbActiveModal.close();
          localStorage.setItem('currentUser',JSON.stringify(data.data));
          this.router.navigate(['/']);
        }
			});
  }

  getType(event){
    const selectEl = event.target;
		this.typeId = selectEl.options[selectEl.selectedIndex].getAttribute('value');
    if(this.typeId == 2){
      this.NgbActiveModal.close();
      var regModal = this.modalService.open(MediasellerComponent);
      regModal.componentInstance.userData = this.userData;
    }else{
      this.NgbActiveModal.close();
      var regModal = this.modalService.open(AdversiterComponent);
      regModal.componentInstance.userData = this.userData;
    }
    
  }
}
