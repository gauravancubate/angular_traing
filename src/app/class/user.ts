export class User {
    mapTo:string;
    email:string;
    contactNo:string;
    password:string;
    isMediaPlayer:boolean;
    firstName: string;
    lastName: string;
    ip: string;
    country:any;
    deviceId: string;
    organization:any;
    userRole: any;
    deviceType: string;
    isFirstTimeLogin:boolean;
    isMobileNoVerified: boolean;
    isEmailVerified: boolean;
    market: any
    userAssetCategory:any;
    businessCategoryId:any;
}
