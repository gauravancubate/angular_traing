import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiserviceService } from '../service/apiservice.service';
import { User } from '../class/user';
import { Md5 } from 'md5-typescript';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { MediasellerComponent } from '../mediaseller/mediaseller.component';

@Component({
	selector: 'app-adversiter',
	templateUrl: './adversiter.component.html',
	styleUrls: ['./adversiter.component.css']
})
export class AdversiterComponent implements OnInit {

	baseUrl = "https://staging.gainbuzz.com:8080/gainbuzz/";
	businessCatObj: any;
	adveForm: FormGroup;
	submitted = false;
	userData: any;
	UserDetail: any;
	error:string;
	typeId;
	constructor(private modalService:NgbModal, private formBuilder: FormBuilder, private apiservice: ApiserviceService, private NgbActiveModal: NgbActiveModal, private router: Router) { }

	ngOnInit() {
		this.adveForm = this.formBuilder.group({
			businessName: ['', Validators.required],
			businessCategory: ['', Validators.required]
		});

		this.getBusinessCat()
	}
	get a() { return this.adveForm.controls; }

	onSubmitAdv() {
		this.submitted = true;
		if (this.adveForm.invalid) {
			return;
		}
		var advForm = this.adveForm.value;

		var apiUrl = this.baseUrl + "guest/registration";
		var isAuth = false;
		var userObj = new User();
		userObj.firstName = this.userData.firstName;
		userObj.lastName = this.userData.lastName;
		userObj.contactNo = this.userData.number;
		userObj.email = this.userData.email;
		userObj.password = Md5.init(this.userData.password);
		userObj.ip = "182.72.105.152";
		userObj.country = {
			id: this.userData.country
		};
		userObj.deviceId = "";
		userObj.organization = {
			id: 0,
			name: advForm.businessName
		};
		userObj.userRole = { id: "3" };
		userObj.deviceType = "WEB";
		userObj.mapTo = "USER";
		userObj.isFirstTimeLogin = true;
		userObj.isMobileNoVerified = false;
		userObj.isEmailVerified = true;
		userObj.businessCategoryId = [advForm.businessCategory];


		this.apiservice.post(apiUrl, userObj, isAuth).subscribe(
			data => {
				this.UserDetail = data;
				if (data.status == 'error') {
					if (data.code == "NOT_FOUND") {
						this.error = "Please try again";
					}
					else {
						this.error = "User already Exits";
					}
				} else {
					this.NgbActiveModal.close();
					localStorage.setItem('currentUser', JSON.stringify(data.data));
					this.router.navigate(['/']);
				}
			});
	}

	getBusinessCat() {
		var apiUrl = this.baseUrl + "guest/getbusinessCategories";
		var isAuth = false;
		this.apiservice.get(apiUrl, isAuth).subscribe(
			category => {
				this.businessCatObj = category;
			});
	}

	getType(event){
		const selectEl = event.target;
			this.typeId = selectEl.options[selectEl.selectedIndex].getAttribute('value');
		if(this.typeId == 2){
		  this.NgbActiveModal.close();
		  var regModal = this.modalService.open(MediasellerComponent);
		  regModal.componentInstance.userData = this.userData;
		}else{
		  this.NgbActiveModal.close();
		  var regModal = this.modalService.open(AdversiterComponent);
		  regModal.componentInstance.userData = this.userData;
		}
		
	  }
}
