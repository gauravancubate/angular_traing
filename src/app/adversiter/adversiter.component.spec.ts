import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdversiterComponent } from './adversiter.component';

describe('AdversiterComponent', () => {
  let component: AdversiterComponent;
  let fixture: ComponentFixture<AdversiterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdversiterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdversiterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
