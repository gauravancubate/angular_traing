import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../service/apiservice.service';
import { User } from '../class/user';
import { Md5 } from "md5-typescript";
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-loginpass',
  templateUrl: './loginpass.component.html',
  styleUrls: ['./loginpass.component.css']
})
export class LoginpassComponent implements OnInit {
  baseUrl = "https://staging.gainbuzz.com:8080/gainbuzz/";
  UserDetail: User[];
  password: string;
  errorMsg: string;
  constructor(private apiservice: ApiserviceService, private router: Router, private NgbActiveModal: NgbActiveModal) { }

  ngOnInit() {

  }


  userlogin() {
    var useremail = localStorage.getItem('tempUser');
    var apiUrl = this.baseUrl + "guest/authenticate";
    var isAuth;

    var userObj = new User();
    userObj.mapTo = "USER";
    userObj.isMediaPlayer = false;
    userObj.email = useremail;
    userObj.password = Md5.init(this.password);

    this.apiservice.post(apiUrl, userObj, isAuth).subscribe(
      data => {
        this.UserDetail = data;
        if (data.status == 'error') {
          if (data.code == "NOT_FOUND") {
            this.errorMsg = "Please enter valid password";
          }
          else {
            this.errorMsg = "enter ";
          }
        } else {
          this.NgbActiveModal.close();
          localStorage.setItem('currentUser', JSON.stringify(data.data));
          this.router.navigate(['/']);
        }
      });


  }
}
