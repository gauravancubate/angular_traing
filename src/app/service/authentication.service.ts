import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(private http: HttpClient) {
      this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
      this.currentUser = this.currentUserSubject.asObservable();
  }
  
  public isAuthenticated(): boolean {
    const token = localStorage.getItem('currentUser');
    // Check whether the token is expired and return
    // true or false
    var isValidToken:boolean;
    if(token){
      isValidToken =true;
    }else{
      isValidToken =false;
    }
    return isValidToken;
  }
  // public get currentUserValue(): any {
  //     return this.currentUserSubject.value;
  // }

  // login(username, password) {
    
  //     return this.http.post("https://staging.gainbuzz.com:8080/gainbuzz/guest/authenticate", { username, password })
  //         .pipe(map(user => {
  //             // store user details and jwt token in local storage to keep user logged in between page refreshes
  //             localStorage.setItem('currentUser', JSON.stringify(user));
  //             this.currentUserSubject.next(user);
  //             return user;
  //         }));
  // }

  // logout() {
  //     // remove user from local storage and set current user to null
  //     localStorage.removeItem('currentUser');
  //     this.currentUserSubject.next(null);
  // }
}
