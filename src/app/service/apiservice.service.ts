import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


let header:HttpHeaders = new HttpHeaders({'Access-Control-Allow-Origin' : '*','Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',"Content-Type":"application/json","Accept":"application/json"});


@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {

  constructor(
    private http: HttpClient,
  ) { }

  post(apiUrl, param, isAuth):Observable<any>{
    return this.http.post(apiUrl, param, {headers:header});
  }

  get(apiUrl, isAuth):Observable<any>{
    if(isAuth){
      var token = JSON.parse(localStorage.getItem('currentUser'));
      header = header.append("Authorization","Bearer "+token.accessToken);
    }
    
    return this.http.get(apiUrl, {headers:header});
  }
}
