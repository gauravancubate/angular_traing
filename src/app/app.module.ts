import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from './layout/layout.module';
import { LayoutmasterModule } from './layoutmaster/layoutmaster.module';
import {FormsModule,ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import {NgbModule, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { SignupModule } from './signup/signup.module';
import { SignupComponent } from './signup/signup.component';
import { ModalComponent } from './modal/modal.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LoginpassComponent } from './loginpass/loginpass.component';
import { AuthGuard } from './helpers/auth.guard';
import { MediasellerComponent } from './mediaseller/mediaseller.component';
import { AdversiterComponent } from './adversiter/adversiter.component';
import { GoolemaptestComponent } from './goolemaptest/goolemaptest.component';
import { AgmCoreModule } from '@agm/core';
import { OtpmodalComponent } from './otpmodal/otpmodal.component';
import { NgOtpInputModule } from  'ng-otp-input';
import { AdvertisingModule } from './advertising/advertising.module';
import { DisplayadverComponent } from './displayadver/displayadver.component';


@NgModule({
  declarations: [
    AppComponent,
    ModalComponent,
    LoginComponent,
    RegisterComponent,
    LoginpassComponent,
    MediasellerComponent,
    AdversiterComponent,
    GoolemaptestComponent,
    OtpmodalComponent,
    DisplayadverComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    LayoutmasterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AdvertisingModule,
    NgbModule,
    SignupModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyCD5AJUHHeDuHayKvXODY7GSW9b_0F7A8I",
      libraries: ["places"]
    }),
    NgOtpInputModule
    
  ],
  providers: [NgbActiveModal,AuthGuard],
  bootstrap: [AppComponent],
  entryComponents: [
   SignupComponent,
   LoginComponent,RegisterComponent,
   LoginpassComponent,
   MediasellerComponent,
   AdversiterComponent,
   GoolemaptestComponent,
   OtpmodalComponent
  ]
})
export class AppModule { }
