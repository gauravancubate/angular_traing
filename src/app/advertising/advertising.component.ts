import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../service/apiservice.service';

@Component({
  selector: 'app-advertising',
  templateUrl: './advertising.component.html',
  styleUrls: ['./advertising.component.css']
})
export class AdvertisingComponent implements OnInit {


  constructor(private apiservice: ApiserviceService, ) { }
  // defaultValue:"Outdoor";
  nearbyAssObj: any = {};
  baseUrl = "https://staging.gainbuzz.com:8080/gainbuzz/";
  AssetsDetail: any;
  page: number = 0;
  perPageRecord = 10;
  cat: string;
  listArr = [];
  pageRecord;
  totalRecord = 0;
  mediaSellerData;
  ngOnInit() {
    this.page = 0;
    this.cat = "INDOOR";
    this.mediaSeller();
    this.getAssList(this.page, this.perPageRecord, this.cat);
  }

  getType(event) {
    this.listArr = [];
    const selectEl = event.target;
    this.cat = selectEl.options[selectEl.selectedIndex].getAttribute('value');
    this.page = 0;
    this.totalRecord = 0
    this.getAssList(this.page, this.perPageRecord, this.cat);
  }


  getAssList(page, total, cat) {
    this.nearbyAssObj.filterQuery = {
      "countryName": [
        "India"
      ],
      "suspended": [
        false
      ],
      "suspendedByAdmin":
        [false],
      "approvelStatus": [1]
    };

    this.nearbyAssObj.filterQueryRange = {};
    this.nearbyAssObj.coreName = cat.toUpperCase();
    this.nearbyAssObj.sortBy = [
      "id"
    ];
    this.nearbyAssObj.sortOrder = [
      "DESC"
    ];
    this.nearbyAssObj.first = page;
    this.nearbyAssObj.total = 10;
    this.nearbyAssObj.searchText = "";
    this.nearbyAssObj.queryFields = "";
    this.nearbyAssObj.proximityFields = "";
    this.nearbyAssObj.querySlop = "";
    this.nearbyAssObj.facetLimit = 100;
    this.nearbyAssObj.facetMinCount = 0;
    this.nearbyAssObj.mapParameters = {
      "latitude": "22.9960",
      "longitude": "72.4997",
      "distance": "2"
    };
    this.nearbyAssObj.rawParameters = [
      {
        "name": "indent",
        "value": "on"
      },
      {
        "name": "wt",
        "value": "json"
      }
    ];
    this.nearbyAssObj.proximitySlop = "";
    var apiUrlForAssets = this.baseUrl + "assets/getNearByAsset";
    var isAuth: boolean = false;

    this.apiservice.post(apiUrlForAssets, this.nearbyAssObj, isAuth).subscribe(
      data => {
        this.AssetsDetail = data.data.list;
        this.AssetsDetail.map(item => {
          this.listArr.push(item);
          return this.listArr;
        });

        this.totalRecord = this.totalRecord + this.AssetsDetail.length
        if (this.totalRecord <= data.page.total) {
          data.page.totalRecord = this.totalRecord;
        } else {
          data.page.totalRecord = data.page.total;
        }
        this.pageRecord = data.page;
        if (data.status == 'error') {

        } else {
          if (data.code == "OK") {
          }
        }
      });
  }

  loadMore() {
    var page = 1;
    this.page = this.page + page;
    this.getAssList(this.page, this.perPageRecord, this.cat)
  }

  mediaSeller() {
		var apiUrl = this.baseUrl + "/guest/getAllAdOptionForSelect";
		var isAuth = false;
		this.apiservice.get(apiUrl, isAuth).subscribe(
			data => {
        this.mediaSellerData = data.data;
        console.log("data.data => ",data.data);
        
    });
    
	}
}
