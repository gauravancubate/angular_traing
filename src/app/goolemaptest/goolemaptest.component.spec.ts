import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoolemaptestComponent } from './goolemaptest.component';

describe('GoolemaptestComponent', () => {
  let component: GoolemaptestComponent;
  let fixture: ComponentFixture<GoolemaptestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoolemaptestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoolemaptestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
