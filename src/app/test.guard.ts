import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TestGuard implements CanActivate {
  constructor(private router:Router)
  {
  }
  canActivate(): boolean {
    var user = JSON.parse(localStorage.getItem('currentUser'));
    if (user) {
      this.router.navigate(['/']);
      return false;
    }
    return true;
    
  }
  
}
