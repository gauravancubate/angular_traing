import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayadverComponent } from './displayadver.component';

describe('DisplayadverComponent', () => {
  let component: DisplayadverComponent;
  let fixture: ComponentFixture<DisplayadverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayadverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayadverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
