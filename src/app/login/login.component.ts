import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RegisterComponent } from '../register/register.component';
import { ApiserviceService } from '../service/apiservice.service';

import { User } from '../class/user';
import { LoginpassComponent } from '../loginpass/loginpass.component';
import { MediasellerComponent } from '../mediaseller/mediaseller.component';
import { OtpmodalComponent } from '../otpmodal/otpmodal.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  // @ViewChild(RegisterComponent,) email;
  constructor(private  NgbActiveModal:NgbActiveModal, private modalService:NgbModal,private apiservice: ApiserviceService) { }
  otpObj:any = {};
  baseUrl = "https://staging.gainbuzz.com:8080/gainbuzz/";
  email: string;
  UserDetail: User[];
  
  ngOnInit() {
  }

  checkEmailExits(){
    
    var userObj = new User();
    userObj.mapTo = "USER";
    
		userObj.contactNo = "";
		userObj.email = this.email;

    var apiUrlUserExits = this.baseUrl + "guest/isUserExists";
    var apiUrlOtpVerify = this.baseUrl + "guest/otpverification?email="+this.email;
		var isAuth;
		
		this.apiservice.post(apiUrlUserExits, userObj, isAuth).subscribe(
			data => {
				this.UserDetail = data;
				if (data.status == 'error') {
					if (data.code == "NOT_FOUND") {
            this.apiservice.get(apiUrlOtpVerify, isAuth).subscribe(
              data => {
                this.UserDetail = data;
                if(data.status=='error'){
                  
                }else{
                  if(data.code == "OK"){
                    this.NgbActiveModal.close();
                    var otpModal = this.modalService.open(OtpmodalComponent);
                    this.otpObj.email = this.email;
                    this.otpObj.otp = data.data;
                    otpModal.componentInstance.otpObj = this.otpObj;
                  }
                }
              });
              
					}
					else {
					}
				} else {
          localStorage.setItem('tempUser',this.email);
          this.NgbActiveModal.close();
          this.modalService.open(LoginpassComponent);
				}
			});
  }
}
