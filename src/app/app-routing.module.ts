import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { LayoutmasterComponent } from './layoutmaster/layoutmaster.component';
import { AuthGuard } from './helpers/auth.guard';
import { TestGuard } from './test.guard';

const routes: Routes = [
    { 
      path:'login',
      component:LayoutComponent,
      loadChildren: './signin/signin.module#SigninModule',
      canActivate:[TestGuard],
    },
    { 
      path: 'signup',
      component:LayoutComponent,
      loadChildren: './signup/signup.module#SignupModule',
      // canActivate:[AuthGuard],
    },
    { 
      path: '',
      component:LayoutmasterComponent,
      loadChildren:'./home/home.module#HomeModule',
      canActivate:[AuthGuard],
    },
    { 
      path: 'advertising',
      component:LayoutComponent,
      loadChildren:'./advertising/advertising.module#AdvertisingModule',
      canActivate:[TestGuard],
    },
    
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }