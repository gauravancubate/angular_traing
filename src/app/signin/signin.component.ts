import { Component, OnInit, ViewChild, Renderer2, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { User } from '../class/user';
import { ApiserviceService } from '../service/apiservice.service';
import { NgbModal, ModalDismissReasons, NgbActiveModal,NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { SignupModule } from '../signup/signup.module';
import { SignupComponent } from '../signup/signup.component';
import { RegisterComponent } from '../register/register.component';
import { MediasellerComponent } from '../mediaseller/mediaseller.component';
import { AdversiterComponent } from '../adversiter/adversiter.component';
import { GoolemaptestComponent } from '../goolemaptest/goolemaptest.component';
import { OtpmodalComponent } from '../otpmodal/otpmodal.component';


@Component({
	selector: 'app-signin',
	templateUrl: './signin.component.html',
	providers: [ApiserviceService,SignupModule],
	styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
	registerForm: FormGroup;
	submitted = false;
	UserDetail: User[];
	email: string;
	baseUrl = "https://staging.gainbuzz.com:8080/gainbuzz/";
	element: any;
	model1: boolean = false; // email
	model2: boolean = false; //registration
	model3: boolean = false; //login
	closeResult: string;
	private modalRef: NgbModalRef;
	constructor(private formBuilder: FormBuilder,private rd: Renderer2, private modalService: NgbModal,
		private apiservice: ApiserviceService, private el: ElementRef, private activeModal: NgbActiveModal ,private signup:SignupModule) {
		this.element = el.nativeElement;
	}

	ngOnInit(): void {
		this.registerForm = this.formBuilder.group({
			firstName: ['', Validators.required],
			lastName: ['', Validators.required],
			email: ['', [Validators.required, Validators.email]],
			password: ['', [Validators.required, Validators.minLength(6)]],
		});
	}

	open() {
		this.modalService.open(AdversiterComponent);
	}

	test(){
		this.modalService.open(GoolemaptestComponent);
	}

	registerComponent(){
		this.modalService.open(OtpmodalComponent);
	}

	mediasellerComponent(){
		this.modalService.open(MediasellerComponent);
	}
	close() {
		this.modalRef.close();
	}

	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}

	

	checkEmailExits() {
		var userObj = new User();
		userObj.mapTo = "USER";
		userObj.contactNo = "";
		userObj.email = this.email;

		var apiUrl = this.baseUrl + "guest/isUserExists";
		var isAuth;
		
		this.apiservice.post(apiUrl, userObj, isAuth).subscribe(
			data => {
				this.UserDetail = data;
				if (data.status == 'error') {
					if (data.code == "NOT_FOUND") {
						this.model2 = true;
						this.model1 = false;
						this.close();
						// this.open(SignupComponent);
					}
					else {
					}
				} else {
					this.close();
					this.model3 = true;
					this.model1 = false;
				}
			});
	}

	login() {
		var userObj: any;
		userObj.email = this.email;
		userObj.mapTo = "USER";
		userObj.isMediaPlayer = true;
		userObj.password = "25d55ad283aa400af464c76d713c07ad";
		userObj.uuid = "5782f07b5fbd4f0e";
		userObj.playerName = "Redmi 5_27_5782f07b5fbd4f0e"
		userObj.contactNo = "";
		var apiUrl = this.baseUrl + "/guest/authenticate";
		var isAuth = false;
		this.apiservice.post(apiUrl, userObj, isAuth).subscribe(
			data => {
				this.UserDetail = data;
			});
	}

}
