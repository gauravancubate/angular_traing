import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './signin.component';

const routes: Routes = [
  {
    path:'',
    component:SigninComponent
  }
];

@NgModule({
//   declarations: [
//    HeaderComponent
// ],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class SigninRoutingModule { }
