import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User} from '../class/user';

const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin' : '*',
    'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
    "Content-Type":"application/json",
    "Accept":"application/json"
  })
};
@Injectable({
  providedIn: 'root'
})


export class SigninService {
  
  constructor(
    private http: HttpClient,
  ) { }
  
   checkUser(userParam:User):Observable<any> {
    return this.http.post("https://staging.gainbuzz.com:8080/gainbuzz/guest/isUserExists", userParam, httpOptions);
  }

}
