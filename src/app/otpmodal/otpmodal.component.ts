import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Md5 } from 'md5-typescript';
import { ApiserviceService } from '../service/apiservice.service';
import { RegisterComponent } from '../register/register.component';

@Component({
  selector: 'app-otpmodal',
  templateUrl: './otpmodal.component.html',
  styleUrls: ['./otpmodal.component.css']
})
export class OtpmodalComponent implements OnInit {
  visibleBtn: boolean;
  duration: number = 60;
  timeLeft: number;
  otpObj;
  otp;
  error;
  seconds;
  interval;
  timeFormat;
  baseUrl = "https://staging.gainbuzz.com:8080/gainbuzz/";
  UserDetail:any;
  constructor(private apiservice:ApiserviceService, private NgbActiveModal:NgbActiveModal, private modalService:NgbModal) { }

  ngOnInit() {
    this.visibleBtn = false;
    this.startTimer();
    
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  onOtpChange(event) {
    event.length
    this.otp = event;
    if (event.length == 6) {
      this.visibleBtn = true;
    } else {
      this.visibleBtn = false;
    }
  }

  startTimer() {
    this.duration = 60;
    if (this.duration > 0) {
      this.interval = setInterval(() => {
        this.duration = this.duration - 1
        if (this.duration <= 0) {
          clearInterval(this.interval)
          // perform next actions
        }
        if (this.duration % 60 < 10) {
          this.seconds = "0" + this.duration % 60
        } else {
          this.seconds = (this.duration % 60).toString()
        }
        this.timeFormat = "00" + " : " + this.seconds;
      }, 1000);
    }
  }

  resend(){
    var apiUrlOtpVerify = this.baseUrl + "guest/otpverification?email="+this.otpObj.email;
		var isAuth;
    this.apiservice.get(apiUrlOtpVerify, isAuth).subscribe(
      data => {
        this.UserDetail = data;
        if(data.status=='error'){
          this.error = "Please enter valid Otp";
        }else{
          if(data.code == "OK"){
             
          }
        }
      });
  }

  checkOtp() {
    if(this.otpObj.otp == Md5.init(Md5.init(this.otp))){
      this.NgbActiveModal.close();
      var otpModal = this.modalService.open(RegisterComponent);
      otpModal.componentInstance.email = this.otpObj.email;
    }else{
      this.error = "Please enter valid Otp";
    }
  }

  

}
