import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutmasterRoutingModule } from './layoutmaster-routing.module';
import { LayoutmasterComponent } from './layoutmaster.component';


@NgModule({
  declarations: [LayoutmasterComponent],
  imports: [
    CommonModule,
    LayoutmasterRoutingModule
  ]
})
export class LayoutmasterModule { }
