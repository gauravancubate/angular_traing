import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutmasterComponent } from './layoutmaster.component';

describe('LayoutmasterComponent', () => {
  let component: LayoutmasterComponent;
  let fixture: ComponentFixture<LayoutmasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutmasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutmasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
