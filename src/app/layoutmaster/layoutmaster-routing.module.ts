import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutmasterComponent } from './layoutmaster.component';

const routes: Routes = [{ path: '', component: LayoutmasterComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutmasterRoutingModule { }
