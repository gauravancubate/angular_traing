import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../service/apiservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-layoutmaster',
  templateUrl: './layoutmaster.component.html',
  styleUrls: ['./layoutmaster.component.css']
})
export class LayoutmasterComponent implements OnInit {
  
  baseUrl = "https://staging.gainbuzz.com:8080/gainbuzz/";
  UserDetail: any;

  constructor(private apiservice:ApiserviceService, private router:Router) { }

  ngOnInit() {
  }

  logout(){
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    var userId = currentUser.id;
    var apiUrl = this.baseUrl + "secure/user/logout?userLogId="+userId;
    var isAuth = true;

  this.apiservice.get(apiUrl, isAuth).subscribe(
    data => {
      this.UserDetail = data;
      localStorage.removeItem('currentUser');
      this.router.navigate(['/login']);
    });
  }
}
