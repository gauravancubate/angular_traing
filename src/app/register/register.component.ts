import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiserviceService } from '../service/apiservice.service';
import { MediasellerComponent } from '../mediaseller/mediaseller.component';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AdversiterComponent } from '../adversiter/adversiter.component';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
	email: string;
	countryCode:string;
	user :any;
	formData:any;
	baseUrl = "https://staging.gainbuzz.com:8080/gainbuzz/";
	countyObj: any;
	registerForm: FormGroup;
	submitted = false;
	userEmail:string;
	constructor(private formBuilder: FormBuilder, private apiservice: ApiserviceService, private modalService: NgbModal, private ngbActive:NgbActiveModal) { }


	ngOnInit() {
		this.registerForm = this.formBuilder.group({
			firstName: ['', Validators.required],
			lastName: ['', Validators.required],
			email: [{value: this.email, disabled: true}, [Validators.required, Validators.email]],
			password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('^(?=[^A-Z]*[A-Z])(?=[^a-z]*[a-z]).{6,}$')]],
			conpassword:['',Validators.required],
			country: ['', Validators.required],
			number:['', Validators.required],
			type:['', Validators.required]
		},{validator: this.checkIfMatchingPasswords('password', 'conpassword')});
		this.userEmail = this.email;
		this.country();
	}
	
	getCountryCode(event) {
		const selectEl = event.target;
		this.countryCode = selectEl.options[selectEl.selectedIndex].getAttribute('data-countryCode');
		
	}
	
	country() {
		var apiUrl = this.baseUrl + "/common/countryList";
		var isAuth = false;
		this.apiservice.get(apiUrl, isAuth).subscribe(
			countrys => {
				this.countyObj = countrys;
		});
	}

	get f() { return this.registerForm.controls; }

	onSubmitReg() {
		this.submitted = true;
		if (this.registerForm.invalid) {
			return;
		}
		
		var type = this.registerForm.get('type').value;
		this.formData = this.registerForm.value;
		this.formData.countryCode = this.countryCode;
		if(type == 'media'){
			this.ngbActive.close();
			var regModal = this.modalService.open(MediasellerComponent);
			regModal.componentInstance.userData = this.formData;
		}else{
			this.ngbActive.close();
			var regModal = this.modalService.open(AdversiterComponent);
			regModal.componentInstance.userData = this.formData;
		}
	}

	numberOnly(event): boolean {
		const charCode = (event.which) ? event.which : event.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}

	checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
		return (group: FormGroup) => {
		  let passwordInput = group.controls[passwordKey],
			  passwordConfirmationInput = group.controls[passwordConfirmationKey];
		  if (passwordInput.value !== passwordConfirmationInput.value) {
			return passwordConfirmationInput.setErrors({notEquivalent: true})
		  }
		  else {
			return passwordConfirmationInput.setErrors(null);
		  }
		}
	  }


	
}
